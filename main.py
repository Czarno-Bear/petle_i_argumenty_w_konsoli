print("Witam!\n Wpisanie trzech jedynek kończy program.\n Dodaj wagę elementu")
element_weight = float(input())  # Waga elementu
new_pack_weight = float(0)  # Waga paczki
quantity_pack = 0  # ilosc paczek
quantity_weight_send = float(0)  # ilosc wyslanych kilogramow
quantity_empty_weight_send = float(0)  # ilosc wyslanych pustych kilogramow
most_empty_pack_weight = float(0)  # najbardziej pusta paczka


while element_weight:  # sprawdzenie czy podano element
    # Jesli trzy jedynki to zakonczenie pracy i wyswietlenie wyniku
    if element_weight == 111:

        #  Ostatnie liczenie i zakończenie programu
        if new_pack_weight != 0:
            quantity_pack += 1
        empty_pack_weight = float(20) - new_pack_weight
        #  zapisanie najbardziej pustej paczki
        if most_empty_pack_weight <= empty_pack_weight:
            most_empty_pack_weight = empty_pack_weight
        #  Obliczenie pustych kilogramow
        quantity_empty_weight_send = quantity_pack * 20 - quantity_weight_send
        # Wyswietlenie wyniku i zakonczenie programu
        print("Zakończono program")
        print("Liczba paczek wysłanych to: {}\n Liczba kilogramów wysłanych to:"
              " {}\n Suma \"Pustych\" kilogramów to: {}\n"
              "Paczka z największą ilością \"Pustych\" kilogramów to: {}"
              .format(quantity_pack, quantity_weight_send,
                      quantity_empty_weight_send, most_empty_pack_weight))
        break
    if 1 <= element_weight <= 10:  # sprawdzenie wagi elementu
        new_pack_weight += element_weight  # dodanie elementu do paczki
        quantity_weight_send += element_weight  # zliczanie kilogramow
        if new_pack_weight > float(20):  # jesli przekroczy wage
            if new_pack_weight != 20:
                new_pack_weight -= element_weight  # odjecie elementu z paczki
            quantity_pack += 1  # dodanie paczki
            #  zainicjowanie pustej paczki
            empty_pack_weight = float(20) - new_pack_weight
            new_pack_weight = element_weight
            #  zapisanie najbardziej pustej paczki
            if most_empty_pack_weight < empty_pack_weight:
                most_empty_pack_weight = empty_pack_weight
        print("Dodaj wagę elementu")  # podanie nowej wagi elementu
        element_weight = float(input())
    else:  # jesli waga nieprawidlowa, zwrocenie bledu
        print("Podano nieprawidłową wagę elementu.\n Podaj wagę elementu")
        element_weight = float(input())
        continue
else:
    print("Podano nieprawidłową wagę elementu.\n Nastąpi zakończenie programu")
